# Fetching data from yahoo finance 

 

## Getting Started

This project let's you fetch data from yahoo finace.

### Prerequisites

```
1) python
2) yahoo_finance API
3) JSON

```

### Installing

```
1) Download and install python.

2) use "pip install yahoo_finance" to install yahoo_finance.

```
### Result

```
['GOOG', 'AAPL', 'MSFT', 'ADBE']
4
    
price of GOOG:
940.49
trade_datetime GOOG:
2017-06-28 20:00:00 UTC+0000
    
price of AAPL:
145.83
trade_datetime AAPL:
2017-06-28 20:00:00 UTC+0000
    
price of MSFT:
69.80
trade_datetime MSFT:
2017-06-28 20:00:00 UTC+0000
    
price of ADBE:
143.00
trade_datetime ADBE:
2017-06-28 16:47:00 UTC+0000
>>> 
```